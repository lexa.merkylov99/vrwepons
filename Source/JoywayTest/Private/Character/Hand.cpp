// Fill out your copyright notice in the Description page of Project Settings.


#include "Character/Hand.h"

#include "DrawDebugHelpers.h"
#include "Character/Interfaces/PlayerInterface.h"
#include "Kismet/KismetSystemLibrary.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
AHand::AHand()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>("Root");

	MotionControllerComponent = CreateDefaultSubobject<UMotionControllerComponent>("MotionController");
	MotionControllerComponent->SetupAttachment(RootComponent);

	HandMesh = CreateDefaultSubobject<USkeletalMeshComponent>("HandMesh");
	HandMesh->SetupAttachment(MotionControllerComponent);

	NozzleRoot = CreateDefaultSubobject<USceneComponent>("NozzleRoot");
	NozzleRoot->SetupAttachment(HandMesh);

	HookCable = CreateDefaultSubobject<UCableComponent>("HookCable");
	HookCable->SetupAttachment(MotionControllerComponent);

	HookEndPoint = CreateDefaultSubobject<USceneComponent>("HookEndPoint");
	HookEndPoint->SetupAttachment(RootComponent);

	HookCable->SetVisibility(false);
}

// Called when the game starts or when spawned
void AHand::BeginPlay()
{
	Super::BeginPlay();
	HookCable->SetAttachEndToComponent(HookEndPoint);
}

// Called every frame
void AHand::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// const FVector LineStartLocation = NozzleRoot->GetComponentLocation();
	// const FVector LineEndLocation = LineStartLocation + NozzleRoot->GetForwardVector() * 25.0f;
	// DrawDebugLine(GetWorld(), LineStartLocation, LineEndLocation, FColor::Red, false,
	//               -1, 0, 1);

	DrawHookLaser();
}

void AHand::PullCableHook()
{
	if (bHookEnable)
	{
		const FVector CableEndStartLocation = HookEndPoint->GetComponentLocation();
		const FVector CableEndLocation = FMath::VInterpConstantTo(CableEndStartLocation, HookHitResult.ImpactPoint,
		                                                          DeltaSTimers, 2500.0f);
		HookEndPoint->SetWorldLocation(CableEndLocation);
		if (CableEndLocation.Equals(HookHitResult.ImpactPoint, 1.0f))
		{
			bHookAttached = true;
			UKismetSystemLibrary::K2_ClearTimer(this, FString("PullCableHook"));
			UKismetSystemLibrary::K2_SetTimer(this, FString("PullPawn"), DeltaSTimers, true);
		}
	}
	else
	{
		UKismetSystemLibrary::K2_ClearTimer(this, FString("PullCableHook"));
	}
}

void AHand::PullPawn()
{
	AActor* SelfOwner = GetOwner();
	if (SelfOwner)
	{
		const float Distance = UKismetMathLibrary::Vector_Distance(MotionControllerComponent->GetComponentLocation(),
		                                                     HookEndPoint->GetComponentLocation());
		const float CalculatedStrenght = (HookForceStrength * FMath::Min(1.0f, (Distance / HookMaxPullDistance) * 2));
		const FVector PullDirection = UKismetMathLibrary::GetDirectionUnitVector(
			MotionControllerComponent->GetComponentLocation(), HookEndPoint->GetComponentLocation());
		const FVector Impulse = PullDirection * HookForceStrength;
		
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green,
								 FString::Printf(TEXT("CalculatedStrenght: %f"), CalculatedStrenght));

		Cast<IPlayerInterface>(SelfOwner)->Execute_AddForceToCharacterMovement(
			SelfOwner, Impulse);
	}
}

void AHand::TryPullHook()
{
	if (!bHookPulled && bHookEnable && HookHitResult.bBlockingHit && HookHitResult.Component.IsValid())
	{
		UKismetSystemLibrary::K2_SetTimer(this, FString("PullCableHook"), DeltaSTimers, true);

		HookEndPoint->AttachToComponent(HookHitResult.GetComponent(), FAttachmentTransformRules::KeepWorldTransform);

		HookEndPoint->SetWorldLocation(MotionControllerComponent->GetComponentLocation());
		HookCable->SetVisibility(true);
		bHookPulled = true;
	}
}

void AHand::CanselHook()
{
	if (bHookPulled && bHookEnable)
	{
		UKismetSystemLibrary::K2_ClearTimer(this, FString("PullCableHook"));
		UKismetSystemLibrary::K2_ClearTimer(this, FString("PullPawn"));

		HookCable->SetVisibility(false);
		bHookPulled = false;
		bHookAttached = false;

		HookEndPoint->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
	}
}

void AHand::DrawHookLaser()
{
	if (!bHookPulled && bHookEnable)
	{
		const FVector LaserStartPoint = MotionControllerComponent->GetComponentLocation();
		const FVector LaserEndPoint = LaserStartPoint + MotionControllerComponent->GetForwardVector() *
			HookMaxPullDistance;

		FColor LaserColor = FColor::Red;

		FCollisionQueryParams TraceQueryParams = FCollisionQueryParams();
		TraceQueryParams.AddIgnoredActor(this);
		TraceQueryParams.AddIgnoredActor(GetOwner());

		if (GetWorld()->LineTraceSingleByChannel(HookHitResult, LaserStartPoint, LaserEndPoint,
		                                         ECollisionChannel::ECC_Visibility, TraceQueryParams))
		{
			LaserColor = FColor::Green;
		}

		DrawDebugLine(GetWorld(), LaserStartPoint, LaserEndPoint, LaserColor,
		              false, -1.0f, 0, 1.0f);
	}
}
