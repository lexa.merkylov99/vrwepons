// Fill out your copyright notice in the Description page of Project Settings.


#include "Character/BaseCharacter.h"

#include "DrawDebugHelpers.h"
#include "HandInterface.h"
#include "InputBehavior.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/KismetSystemLibrary.h"

// GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Green, FString::Printf(TEXT("%f"),	Axis));

// Sets default values
ABaseCharacter::ABaseCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	VROrigin = CreateDefaultSubobject<USceneComponent>(TEXT("VROrigin"));
	VROrigin->SetupAttachment(RootComponent);

	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	Camera->SetupAttachment(VROrigin);

	WidgetInteractionComponent = CreateDefaultSubobject<UWidgetInteractionComponent>(TEXT("WidgetInteraction"));
	WidgetInteractionComponent->SetupAttachment(VROrigin);
}

// Called when the game starts or when spawned
void ABaseCharacter::BeginPlay()
{
	Super::BeginPlay();
	SpawnHands();
}

// Called every frame
void ABaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	DeltaS = DeltaTime;
	UpdatePlayerLocationByCamera();
}

// Called to bind functionality to input
void ABaseCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &ABaseCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ABaseCharacter::MoveRight);
	PlayerInputComponent->BindAxis("RotateInput", this, &ABaseCharacter::RotatePawn);

	PlayerInputComponent->BindAction("TriggerRight", IE_Pressed, this, &ABaseCharacter::TriggerRightPressed);
	PlayerInputComponent->BindAction("TriggerRight", IE_Released, this, &ABaseCharacter::TriggerRightReleased);
	PlayerInputComponent->BindAction("TriggerLeft", IE_Pressed, this, &ABaseCharacter::TriggerLeftPressed);
	PlayerInputComponent->BindAction("TriggerLeft", IE_Released, this, &ABaseCharacter::TriggerLeftReleased);

	PlayerInputComponent->BindAction("GrabLeft", IE_Pressed, this, &ABaseCharacter::GrabLeftPressed);
	PlayerInputComponent->BindAction("GrabRight", IE_Pressed, this, &ABaseCharacter::GrabRightPressed);

	PlayerInputComponent->BindAction("GrabLeft", IE_Released, this, &ABaseCharacter::GrabLeftReleased);
	PlayerInputComponent->BindAction("GrabRight", IE_Released, this, &ABaseCharacter::GrabRightReleased);
}

void ABaseCharacter::MoveForward(float Axis)
{
	ValidateAxisInput(Axis);

	if (Axis != 0.0f)
	{
		const FVector Direction = FVector(Camera->GetForwardVector().X, Camera->GetForwardVector().Y, 0.0f);
		AddMovementInput(Direction, Axis);
	}
}

void ABaseCharacter::MoveRight(float Axis)
{
	ValidateAxisInput(Axis);

	if (Axis != 0.0f)
	{
		const FVector Direction = FVector(Camera->GetRightVector().X, Camera->GetRightVector().Y, 0.0f);
		AddMovementInput(Direction, Axis);
	}
}

void ABaseCharacter::RotatePawn(float Axis)
{
	ValidateAxisInput(Axis);
	if (Axis != 0.0f)
	{
		AddControllerYawInput(Axis);
	}
}

void ABaseCharacter::ValidateAxisInput(float& Axis)
{
	if (abs(Axis) > DeathZoneDown)
	{
		if (Axis > 0.0f)
		{
			if (Axis > DeathZoneTop)
				Axis = 1.0f;
		}
		else
		{
			if (abs(Axis) > DeathZoneTop)
				Axis = -1.0f;
		}
	}
	else
	{
		Axis = 0.0f;
	}
}

void ABaseCharacter::SpawnHands()
{
	// Right hand 
	RightHand = GetWorld()->SpawnActorDeferred<AHand>(HandClass, FTransform::Identity, this, this);
	RightHand->HandSide = EHandSide::Right;
	RightHand->FinishSpawning(FTransform::Identity);
	RightHand->AttachToComponent(VROrigin, FAttachmentTransformRules::KeepRelativeTransform);

	WidgetInteractionComponent->AttachToComponent(RightHand->MotionControllerComponent,
	                                              FAttachmentTransformRules::KeepRelativeTransform);

	// Left Hand
	LeftHand = GetWorld()->SpawnActorDeferred<AHand>(HandClass, FTransform::Identity, this, this);
	LeftHand->HandSide = EHandSide::Left;
	LeftHand->FinishSpawning(FTransform::Identity);
	LeftHand->AttachToComponent(VROrigin, FAttachmentTransformRules::KeepRelativeTransform);
}

void ABaseCharacter::TriggerRightPressed()
{
	if (RightHand != nullptr)
	{
		Cast<IHandInterface>(RightHand)->Execute_TriggerPressed(RightHand);
		RightHand->TryPullHook();
	}

	if (WidgetInteractionComponent->bEnableHitTesting)
	{
		WidgetInteractionComponent->PressPointerKey(EKeys::LeftMouseButton);
	}
}

void ABaseCharacter::TriggerRightReleased()
{
	if (RightHand != nullptr)
	{
		Cast<IHandInterface>(RightHand)->Execute_TriggerReleased(RightHand);
		RightHand->CanselHook();
	}

	WidgetInteractionComponent->ReleasePointerKey(EKeys::LeftMouseButton);
}

void ABaseCharacter::TriggerLeftPressed()
{
	if (LeftHand != nullptr)
	{
		Cast<IHandInterface>(LeftHand)->Execute_TriggerPressed(LeftHand);
		LeftHand->TryPullHook();
	}
}

void ABaseCharacter::TriggerLeftReleased()
{
	if (LeftHand != nullptr)
	{
		Cast<IHandInterface>(LeftHand)->Execute_TriggerReleased(LeftHand);
		LeftHand->CanselHook();
	}
}

void ABaseCharacter::GrabRightPressed()
{
	if (RightHand != nullptr)
	{
		Cast<IHandInterface>(RightHand)->Execute_GrabActor(RightHand);

		// GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Flying);

		bGripRightPressed = true;
		TryActivateFlyingMode();
	}
}

void ABaseCharacter::GrabLeftPressed()
{
	if (LeftHand != nullptr)
	{
		Cast<IHandInterface>(LeftHand)->Execute_GrabActor(LeftHand);

		bGripLeftPressed = true;
		TryActivateFlyingMode();
	}
}

void ABaseCharacter::GrabRightReleased()
{
	if (RightHand != nullptr)
	{
	}

	bGripRightPressed = false;
	TryDeactivateFlyingMode();
}

void ABaseCharacter::GrabLeftReleased()
{
	if (LeftHand != nullptr)
	{
	}

	bGripLeftPressed = false;
	TryDeactivateFlyingMode();
}

void ABaseCharacter::IronManFlying()
{
	GetCharacterMovement()->AddForce(GetLeftHandNozzleForce() + GetRightHandNozzleForce());
	GetCharacterMovement()->AddForce(GetGravityForce());

	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green,
	                                 FString::Printf(TEXT("GravityForce: %f"), GetGravityForce().Z));
}

void ABaseCharacter::TryDeactivateFlyingMode()
{
	if (!bGripLeftPressed && !bGripRightPressed)
	{
		GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Walking);
		UKismetSystemLibrary::K2_ClearTimer(this, FString("IronManFlying"));
	}
}

void ABaseCharacter::TryActivateFlyingMode()
{
	if (!UKismetSystemLibrary::K2_IsTimerActive(this, FString("IronManFlying")) && CheckDownForceOvercome())
	{
		GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Flying);
		UKismetSystemLibrary::K2_SetTimer(this, FString("IronManFlying"), 1.0f / 60.0f, true);
	}
}

FVector ABaseCharacter::GetRightHandNozzleForce()
{
	if (RightHand == nullptr)
		return FVector::ZeroVector;

	const FVector NozzleForward = RightHand->NozzleRoot->GetForwardVector() * -1.0f;
	const FVector NozzleForce = NozzleForward * RightHand->NozzleForceStrength;

	return bGripRightPressed ? NozzleForce : FVector::ZeroVector;
}

FVector ABaseCharacter::GetLeftHandNozzleForce()
{
	if (LeftHand == nullptr)
		return FVector::ZeroVector;

	const FVector NozzleForward = LeftHand->NozzleRoot->GetForwardVector() * -1.0f;
	const FVector NozzleForce = NozzleForward * LeftHand->NozzleForceStrength;

	return bGripLeftPressed ? NozzleForce : FVector::ZeroVector;
}

FVector ABaseCharacter::GetGravityForce()
{
	float GravityForce = 0;
	if (RightHand)
	{
		GravityForce += RightHand->NozzleForceStrength * -0.7f;
		GravityForce += GetRightHandNozzleForce().Z;
	}

	if (LeftHand)
	{
		GravityForce += LeftHand->NozzleForceStrength * -0.7f;
		GravityForce += GetLeftHandNozzleForce().Z;
	}

	return FVector(0.0f, 0.0f, FMath::Min(GravityForce, 0.0f));
}

bool ABaseCharacter::CheckDownForceOvercome()
{
	return GetGravityForce().Z >= -1.0f || GetCharacterMovement()->IsFalling();
}

void ABaseCharacter::UpdatePlayerLocationByCamera()
{
	FVector VROriginLocation = VROrigin->GetComponentLocation();

	FVector NewPlayerLocation = Camera->GetComponentLocation();
	NewPlayerLocation.Z = GetActorLocation().Z;

	SetActorLocation(NewPlayerLocation, true);
	VROrigin->SetWorldLocation(VROriginLocation);
}

void ABaseCharacter::SetEnabledWidgetInteraction_Implementation(bool Enabled)
{
	IPlayerInterface::SetEnabledWidgetInteraction_Implementation(Enabled);
	WidgetInteractionComponent->bEnableHitTesting = Enabled;
}

void ABaseCharacter::AddForceToCharacterMovement_Implementation(FVector Force)
{
	IPlayerInterface::AddForceToCharacterMovement_Implementation(Force);
	GetCharacterMovement()->Velocity = GetCharacterMovement()->Velocity *VelocityDamping;
	GetCharacterMovement()->AddImpulse(Force);
}

// void ABaseCharacter::AddForceToCharacterMovement(FVector Force)
// {
// 	IPlayerInterface::AddForceToCharacterMovement(Force);
// 	GetCharacterMovement()->AddForce(Force);
// }

void ABaseCharacter::Destroyed()
{
	Super::Destroyed();
	if (LeftHand != nullptr)
		LeftHand->Destroy();
	if (RightHand != nullptr)
		RightHand->Destroy();
}
