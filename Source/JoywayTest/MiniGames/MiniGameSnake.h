// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Kismet/KismetRenderingLibrary.h"
#include "MiniGameSnake.generated.h"

UCLASS()
class JOYWAYTEST_API AMiniGameSnake : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMiniGameSnake();
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UStaticMeshComponent* SM_Screen;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSoftObjectPtr<UTextureRenderTarget2D> RT_Screen;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UCanvas* RT_Canvas;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector2D PixelSize;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int MaxWidthPixel = 64;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int MaxHeightPixel = 64;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int RtSizeWidth = 32;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int RtSizeHeight = 32;
	
	FVector2D CanvasSize;
	FDrawToRenderTargetContext RT_Context;
	UFUNCTION(BlueprintCallable)
	void DrawPixel(const FVector2D ScreenPosition,const FLinearColor PixelColor);
	
};
