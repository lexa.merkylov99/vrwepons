// Fill out your copyright notice in the Description page of Project Settings.


#include "MiniGameSnake.h"

#include "Engine/Canvas.h"
#include "Engine/TextureRenderTarget2D.h"
#include "Kismet/KismetRenderingLibrary.h"

// Sets default values
AMiniGameSnake::AMiniGameSnake()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	RootComponent = CreateDefaultSubobject<USceneComponent>("Root");

	SM_Screen = CreateDefaultSubobject<UStaticMeshComponent>("SM_Screen");
	SM_Screen->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AMiniGameSnake::BeginPlay()
{
	Super::BeginPlay();
	if (RT_Screen.IsValid())
	{
		RtSizeWidth = RT_Screen->SizeX;
		RtSizeHeight = RT_Screen->SizeY;
		PixelSize.Set(RtSizeWidth / MaxWidthPixel, RtSizeHeight / MaxHeightPixel);


		CanvasSize = FVector2D(RtSizeWidth, RtSizeHeight);

		UKismetRenderingLibrary::BeginDrawCanvasToRenderTarget(GetWorld(), RT_Screen.Get(), RT_Canvas, CanvasSize,
		                                                       RT_Context);
		
	}
}

// Called every frame
void AMiniGameSnake::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AMiniGameSnake::DrawPixel(const FVector2D ScreenPosition, const FLinearColor PixelColor)
{
	if (!RT_Canvas)
		return;

	UTexture* NullTexture = nullptr;
	RT_Canvas->K2_DrawTexture(NullTexture, ScreenPosition, PixelSize, FVector2D::ZeroVector, FVector2D(1.0f, 1.0f),
	                          PixelColor);

}
