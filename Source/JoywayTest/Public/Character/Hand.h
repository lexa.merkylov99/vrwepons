// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MotionControllerComponent.h"
#include "GameFramework/Actor.h"
#include "CableComponent.h"
#include "Hand.generated.h"

UENUM(BlueprintType)
enum class EHandSide : uint8
{
	Right = 0 UMETA(DisplayName = "Right"),
	Left = 1 UMETA(DisplayName = "Left"),
};

UCLASS()
class JOYWAYTEST_API AHand : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AHand();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Component")
	UMotionControllerComponent* MotionControllerComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Component")
	USkeletalMeshComponent* HandMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Component")
	USceneComponent* NozzleRoot;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Component")
	UCableComponent* HookCable;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Component")
	USceneComponent* HookEndPoint;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EHandSide HandSide;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float NozzleForceStrength = 35000.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float HookForceStrength = 35000.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bHookPulled;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bHookEnable = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bHookAttached;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float HookMaxPullDistance = 3500.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FHitResult HookHitResult;

	UPROPERTY()
	float DeltaSTimers = 1.0f / 60.0f;

	UFUNCTION()
	void PullCableHook();

	UFUNCTION()
	void PullPawn();

	UFUNCTION()
	void TryPullHook();

	UFUNCTION()
	void CanselHook();

	UFUNCTION()
	void DrawHookLaser();

};
